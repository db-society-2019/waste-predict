# coding=utf-8
import os, sys
import numpy as np

import tensorflow as tf
from keras.models import load_model
from keras.preprocessing import image
from keras.applications.imagenet_utils import preprocess_input

os.environ["CUDA_VISIBLE_DEVICES"] = ""

classes = np.array([ "가방", "가스레인대", "가스오븐레인지", "간이옷장", "간판", "개수대", "거울", "게시판", "경조사용화환", "고무통", "골프가방", "공기청정기", "교자상", "기름탱크", "기타목재", "기타플라스틱", "김치냉장고", "난로", "냉장고", "다리미판", "도마", "돌침대", "돗자리", "등산스틱", "러닝머신", "문갑", "문짝", "물탱크", "밥상", "방석", "배기후드", "벽걸이시계", "변기통", "병풍", "보일러", "보행기", "복사기", "블라인드", "비데", "빨래건조대", "서랍장", "선풍기", "세단기", "세면대", "세탁기", "소파류", "수도호스", "수족관", "스케이트", "스키", "스피커", "식기건조기", "식탁", "신발장", "싱크대", "쌀통", "쓰레기통", "아이스박스", "액자", "에어콘", "오디오", "오디오장식장", "오락기", "오르간", "옥매트", "옷걸이", "욕조", "유모차", "유아용목마", "의자", "이불등", "인라인", "인형류", "입간판", "자동판매기", "자전거", "장난감류", "장롱", "장식장", "장판", "재봉틀", "전기담요", "전기판넬", "정수기", "조명기구", "진열대", "차탁자", "찬장", "책꽂이", "책상", "책장", "천막", "청소기", "침대", "카펫", "캐비닛", "컴퓨터책상", "킥보드", "타이어", "탁구대", "테니스라켓", "텐트", "텔레비전대", "텔레비젼", "파일캐비닛", "파티션", "팬히터", "피아노", "항아리", "행거", "헬스자전거", "협탁", "화분", "화장대", "화장품함", "환풍기", "회의용탁자", "휠체어", "히터" ])

def init():
  model = load_model("./models/inception_v4_20190208_144038.h5")
  print("Loaded Model from disk")

  graph = tf.get_default_graph()

  return model, graph

def predict(model, graph, img_file, size):
  results = []

  with graph.as_default():
    img = image.load_img(img_file, target_size=(size, size))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    preds = model.predict(x)
    preds = np.array(preds[0])
    top_n_preds = preds.argsort()[-9:][::-1]

    for t_idx in top_n_preds:
      results.append({
        "name": classes[t_idx],
        "acc": preds[t_idx].item()
        })

    return results
