# coding=utf-8

import os, os.path, sys, getopt
import numpy as np
import json

from flask import render_template, request, jsonify

from app.predict import init, predict

from app import app
global model, graph
model, graph = init()

@app.route('/')
@app.route('/index')
def index():
  return render_template('index.html')

@app.route('/upload', methods = ['POST'])
def upldfile():
  if request.method == 'POST':
    size = 299
    uploaded_file = request.files['file']
    results = predict(model, graph, uploaded_file, size)

    return jsonify(results=results)
